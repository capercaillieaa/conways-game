﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Conway_s_game
{
    public partial class GameForm : Form
    {
        private enum GameMode
        {
            Discrete,
            Continuous
        };
        private const int NUM_OF_COLUMNS = 35;
        private const int NUM_OF_ROWS = 30;
        private readonly bool[,] cells;
        private bool stopFlag;
        public GameForm()
        {
            InitializeComponent();
            cells = new bool[NUM_OF_ROWS, NUM_OF_COLUMNS];
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            ClearButton_Click(null, null);
            FillGameFiled();
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.Cancel)
            {
                ClearButton_Click(null, null);
                string[] cellsData = ReadCellsData(openFileDialog.FileName);
                FillCells(cellsData);
                FillGameFiled();
            }  
        }

        private string[] ReadCellsData(string fileName)
            => File.ReadAllLines(fileName);

        private void FillCells(string[] cellsData)
        {
            for (int i = 0; i < NUM_OF_ROWS; i++)
                for (int j = 0; j < NUM_OF_COLUMNS; j++)
                    if (cellsData[i][j] == '1') cells[i, j] = true;
                    else cells[i, j] = false;
        }

        private void FillGameFiled()
        {
            Color cellColor;
            GameField.Controls.Clear();
            for (int i = 0; i < NUM_OF_ROWS; i++)
                for (int j = 0; j < NUM_OF_COLUMNS; j++)
                {
                    if (cells[i, j]) cellColor = Color.Snow;
                    else cellColor = Color.Gray;
                    var cell = CreateCell(cellColor);
                    GameField.Controls.Add(cell, j, i);
                }       
        }

        private Button CreateCell(Color cellColor)
        {
            var cell = new Button()
            {
                BackColor = cellColor,
                Dock = DockStyle.Fill,
                FlatStyle = FlatStyle.Flat,
                Margin = Padding.Empty
            };
            cell.FlatAppearance.BorderColor = Color.Silver;
            cell.Click += CellClick;
            return cell;
        }

        private void CellClick(object sender, EventArgs e)
        {
            StepLabel.Text = "0";
            var cell = sender as Button;
            if (cell.BackColor == Color.Snow)
                ChangeCell(cell, Color.Gray, false);
            else
                ChangeCell(cell, Color.Snow, true);
        }

        private void ChangeCell(Button cell, Color cellColor, bool isActive)
        {
            (var row, var column) = GetCellPosition(cell);
            cell.BackColor = cellColor;
            cells[row, column] = isActive;
        }

        private (int, int) GetCellPosition(Button cell)
            => (GameField.GetRow(cell), GameField.GetColumn(cell));

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() != DialogResult.Cancel)
            {
                var cellsData = GetCellsData();
                WriteCellsData(saveFileDialog.FileName, cellsData);
            }
        }

        private string[] GetCellsData()
        {
            var cellsData = new string[NUM_OF_ROWS];
            for (int i = 0; i < NUM_OF_ROWS; i++)
                for (int j = 0; j < NUM_OF_COLUMNS; j++)
                    if (cells[i, j]) cellsData[i] += '1';
                    else cellsData[i] += '0';
            return cellsData;
        }

        private void WriteCellsData(string fileName, string[] cellsData)
            => File.WriteAllLines(fileName, cellsData);

        private void StepButton_Click(object sender, EventArgs e)
        {
            ChangeStepLabel(StepLabel.Text);
            TakeStep(GameMode.Discrete);
        }

        private async void StartButton_Click(object sender, EventArgs e)
        {
            stopFlag = false;
            while (!stopFlag)
            {
                ChangeStepLabel(StepLabel.Text);
                await Task.Run(() => TakeStep(GameMode.Continuous));
            }
        }

        private void ChangeStepLabel(string text)
            => StepLabel.Text = (int.Parse(text) + 1).ToString();

        private void TakeStep(GameMode mode)
        {
            for (int i = 0; i < NUM_OF_ROWS; i++)
                for (int j = 0; j < NUM_OF_COLUMNS; j++)
                {
                    var cell = GetButtonFromGameField(j, i);
                    var numOfNeighbors = GetNumberOfNeighbors(i, j);
                    if (cells[i, j] && (numOfNeighbors > 3 || numOfNeighbors <= 1))
                        ChangeCell(cell, Color.Gray, false);
                    else if (!cells[i, j] && numOfNeighbors == 3)
                        ChangeCell(cell, Color.Snow, true);
                }
            if (mode == GameMode.Continuous)
            {
                MakeFrame();
                RefreshGameField();
            }
        }

        private void RefreshGameField()
        {
            if (GameField.InvokeRequired)
                GameField.BeginInvoke(new Action(GameField.Refresh));
        }

        private void MakeFrame()
        {
            Thread.Sleep(10);
        }

        private Button GetButtonFromGameField(int row, int column)
            => GameField.GetControlFromPosition(row, column) as Button;

        private int GetNumberOfNeighbors(int row, int column)
        {
            var neigborsCount = 0;
            for (int i = row - 1; i <= row + 1; i++)
                for (int j = column - 1; j <= column + 1; j++)
                {
                    if ((i == row && j == column) 
                        || i < 0 || i >= NUM_OF_ROWS 
                        || j < 0 || j >= NUM_OF_COLUMNS) continue;
                    if (cells[i, j]) neigborsCount++;
                }
            return neigborsCount;
        }

        private void StopButton_Click(object sender, EventArgs e)
            => stopFlag = true;

        private void ClearButton_Click(object sender, EventArgs e)
        {
            ChangeStepLabel("-1");
            for (int i = 0; i < NUM_OF_ROWS; i++)
                for (int j = 0; j < NUM_OF_COLUMNS; j++)
                {
                    var cell = GameField.GetControlFromPosition(j, i) as Button;
                    if (cell != null) cell.BackColor = Color.Gray;
                    cells[i, j] = false;
                }
        }
    }
}
